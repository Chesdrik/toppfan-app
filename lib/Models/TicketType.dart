class TicketType {
  int id;
  String seatTypeName;
  String ticketColor;
  double price;
  int active;
  int amount;

  TicketType(this.id, this.seatTypeName, this.ticketColor, this.price,
      this.active, this.amount);

  Map<String, dynamic> toJson() => {
        'id': id,
        'seatTypeName': seatTypeName,
        'ticketColor': ticketColor,
        'price': price,
        'active': active,
        'amount': amount,
      };
}
