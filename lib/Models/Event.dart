class Event {
  int id;
  String name;
  String date;
  int venueId;
  String img;
  // List<Ticket> tickets;

  Event({
    this.id,
    this.name,
    this.date,
    this.venueId,
    this.img,
    // this.tickets,
  });

  factory Event.fromMap(Map<String, dynamic> json) => new Event(
        id: json["id"],
        name: json["name"],
        date: json["date"],
        venueId: json["venueId"],
        img: json["img"],
        // tickets: json["tickets"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'date': date,
        'venueId': venueId,
        'img': img,
        // 'tickets': tickets,
      };
}
