class User {
  int id;
  String name;
  String email;
  String type;
  int active;

  User({this.id, this.name, this.email, this.type, this.active});

  factory User.fromMap(Map<String, dynamic> json) => new User(
      id: json["id"],
      name: json["name"],
      email: json["email"],
      type: json["type"],
      active: json["active"]);

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "email": email,
        "type": type,
        "active": active,
      };
}
