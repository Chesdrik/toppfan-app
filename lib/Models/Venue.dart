class Venue {
  int id;
  String name;
  double lat;
  double long;
  String img;

  Venue({
    this.id,
    this.name,
    this.lat,
    this.long,
    this.img,
  });

  factory Venue.fromMap(Map<String, dynamic> json) => new Venue(
        id: json["id"],
        name: json["name"],
        lat: json["lat"],
        long: json["long"],
        img: json["img"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'lat': lat,
        'long': long,
        'img': img,
      };
}
