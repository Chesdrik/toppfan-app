class Ticket {
  int id;
  int eventId;
  int amount;
  String ticketTypeName;
  String ticketTypeColor;
  String ticketTypeAccess;
  String qr;

  Ticket({
    this.id,
    this.eventId,
    this.amount,
    this.ticketTypeName,
    this.ticketTypeColor,
    this.ticketTypeAccess,
    this.qr,
  });

  factory Ticket.fromMap(Map<String, dynamic> json) => new Ticket(
        id: json["id"],
        eventId: json["eventId"],
        amount: json["amount"],
        ticketTypeName: json["ticketTypeName"],
        ticketTypeColor: json["ticketTypeColor"],
        ticketTypeAccess: json["ticketTypeAccess"],
        qr: json["qr"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'eventId': eventId,
        'amount': amount,
        'ticketTypeName': ticketTypeName,
        'ticketTypeColor': ticketTypeColor,
        'ticketTypeAccess': ticketTypeAccess,
        'qr': qr,
      };
}
