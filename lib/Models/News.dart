class News {
  String title;
  String date;
  String content;
  String img;
  String url;

  News({
    this.title,
    this.date,
    this.content,
    this.img,
    this.url,
  });

  factory News.fromMap(Map<String, dynamic> json) => new News(
        title: json["title"],
        date: json["date"],
        content: json["content"],
        img: json["img"],
        url: json["url"],
      );

  Map<String, dynamic> toMap() => {
        'title': title,
        'date': date,
        'content': content,
        'img': img,
        'url': url,
      };
}
