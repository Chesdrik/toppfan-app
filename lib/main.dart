import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import 'package:toppfan_app/Views/LoadingHome.dart';
import 'package:toppfan_app/Views/Home.dart';
import 'package:toppfan_app/Views/Login.dart';
import 'package:toppfan_app/Views/Account.dart';
import 'package:toppfan_app/Views/NewsList.dart';
import 'package:toppfan_app/Views/Admin/AdminDashboard.dart';
import 'package:toppfan_app/Views/Events/EventsList.dart';
import 'package:toppfan_app/Views/Events/EventView.dart';
import 'package:toppfan_app/Views/Events/TicketsList.dart';
import 'package:toppfan_app/Views/Privacy.dart';

void main() => runApp(
      Sizer(
        builder: (context, orientation, deviceType) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            initialRoute: '/',
            routes: {
              '/': (context) => LoadingHome(),
              '/home': (context) => Home(),
              '/login': (context) => Login(),
              '/account': (context) => Account(),
              '/news': (context) => NewsList(),
              '/events': (context) => EventsList(),
              '/events/tickets': (context) => EventView(),
              '/dashboard/admin': (context) => AdminDashboard(),
              '/my_tickets': (context) => TicketsList(),
              '/privacy': (context) => Privacy(),

              // '/dashboard/client': (context) => ClientDashboard(),
              // '/partidos': (context) => PartidosboFetch(),
              // '/event': (context) => EventFetch(),
              // '/account': (context) => Account(),
              // '/ticket_dash': (context) => TicketsFetch(),
              // '/loyalty': (context) => Loyalty(),
              // //access
              // '/access_dash': (context) => AccessDash(),
            },
          );
        },
      ),
    );
