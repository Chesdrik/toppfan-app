part of api_library;

Future<void> syncTickets(context) async {
  print("Sync tickets");
  // Fetching user from local storage
  User user = await getGlobalGlobalUser();

  // Making API call
  var jsonData = await requestPOST("/app/tickets", {
    'client_user_id': (user != null ? user.id.toString() : 'na'),
  });

  print("Data fetched");
  print(jsonData);

  // If there is no error, we process data
  if (jsonData == null) {
    print('Offline!');
  } else if (jsonData['error'] == false) {
    print("No errors");
    // Removing tickets from local db
    await TicketDBrovider.db.database;
    TicketDBrovider.db.deleteAll();

    // Parse tickets
    for (var jsonEvent in jsonData['tickets']) {
      // Creating event in case it dosn't exists
      if (!await EventDBrovider.db
          .eventExists(jsonEvent['event']['id'].toString())) {
        print("Adding event");
        EventDBrovider.db.newEvent(Event.fromMap(jsonEvent['event']));
      }

      // Creating venue in case we don't have it already
      if (!await VenueDBrovider.db.venueExists(jsonEvent['venue']['id'])) {
        print('Venue with id ' +
            jsonEvent['venue']['id'].toString() +
            ' does not exist');
        VenueDBrovider.db.newVenue(Venue.fromMap(jsonEvent['venue']));
      }

      // Creating tickets
      for (var jsonTicket in jsonEvent['tickets']) {
        TicketDBrovider.db.newTicket(Ticket.fromMap(jsonTicket));
      }
    }
  } else {
    // Error fetching tickets
    Alert(
      context: context,
      type: AlertType.error,
      title: "Error",
      desc: jsonData['error_message'],
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
  }

  // Returning function
  return;
}
