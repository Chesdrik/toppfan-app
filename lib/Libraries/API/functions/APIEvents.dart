part of api_library;

Future<void> syncEvents(context) async {
  // Fetching user from local storage
  User user = await getGlobalGlobalUser();

  // Making API call
  var jsonData = await requestPOST("/app/events", {
    'client_user_id': (user != null ? user.id.toString() : 'na'),
  });

  print("syncEvents");
  print(jsonData);
  print("Error: ");
  print(jsonData['error']);
  print("-------------");

  // If there is no error, we process data
  if (jsonData == null) {
    print('Offline!');
  } else if (jsonData['error'] == false) {
    // Cleaning DB
    await EventDBrovider.db.database;
    EventDBrovider.db.deleteAll();

    await VenueDBrovider.db.database;
    VenueDBrovider.db.deleteAll();

    // Parse tickets
    for (var jsonEvent in jsonData['events']) {
      // Creating event
      EventDBrovider.db.newEvent(Event.fromMap(jsonEvent['event']));

      // Creating venue in case we don't have it already
      if (!await VenueDBrovider.db.venueExists(jsonEvent['venue']['id'])) {
        print('Venue with id ' +
            jsonEvent['venue']['id'].toString() +
            ' does not exist');
        VenueDBrovider.db.newVenue(Venue.fromMap(jsonEvent['venue']));
      }
    }
  } else {
    // Error fetching tickets
    Alert(
      context: context,
      type: AlertType.error,
      title: "Error",
      desc: jsonData['error_message'],
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
  }

  // Returning function
  return;
}
