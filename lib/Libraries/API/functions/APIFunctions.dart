part of api_library;

// Function for creaging URI
Uri uriConstructor(endpoint) {
  if (useHttps) {
    return Uri.https(baseURL, urlSuffix + endpoint);
  } else {
    return Uri.http(baseURL, urlSuffix + endpoint);
  }
}

// Fetching data using POST
Future<Map<String, dynamic>> requestPOST(
    String requestURL, Map<String, dynamic> arguments) async {
  Uri uri = uriConstructor(requestURL);
  print('URL = ' + uri.toString());
  print('Arguments = ' + jsonEncode(arguments));

  var response;

  // Fetching auth key
  var authKey = await getGlobalAuthKey();

  // Fetching response from URL
  try {
    response = await http
        .post(uri,
            body: Map<String, dynamic>.from(arguments),
            headers: (authKey == null
                ? {}
                : {
                    "Authorization": "bearer " + authKey,
                  }))
        .timeout(const Duration(seconds: 15));
  } on TimeoutException catch (e) {
    // print('Timeout');
    print(e);
    return null;
  } catch (e) {
    // print('otro');
    print(e);
    return null;
  }

  // print(response.body.toString());

  var jsonData = jsonDecode("{}");
  if (response.statusCode == 200) {
    jsonData = jsonDecode(response.body);
  }

  return jsonData;
}

// Fetching data using GET
Future<Map<String, dynamic>> requestGET(String requestURL) async {
  print("GET:" + requestURL);
  Uri uri = uriConstructor(requestURL);

  // Fetching auth key
  var authKey = await getGlobalAuthKey();

  // Fetching response from URL
  var response = (authKey == null
      ? await http.get(uri)
      : await http.get(uri, headers: {"Authorization": "bearer " + authKey}));

  var jsonData = jsonDecode("{}");
  if (response.statusCode == 200) {
    jsonData = jsonDecode(response.body);
  }

  print(jsonData);

  return jsonData;
}
