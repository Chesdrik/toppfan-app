part of api_library;

Future<void> getPrivacy(context) async {
  print('Fetching privacy inside yeaaah');
  // Making API call
  var jsonData = await requestPOST("/app/privacy", {});

  if (jsonData['error'] == false) {
    globalPrivacy = jsonData['privacy'];
  } else {
    // Error fetching tickets
    Alert(
      context: context,
      type: AlertType.error,
      title: "Error",
      desc: 'Hay problemas de conexión con el servidor',
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
  }
}
