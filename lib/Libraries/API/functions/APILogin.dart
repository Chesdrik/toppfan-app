part of api_library;

Future apiLogin(context, email, password) async {
  // Saving user and password to shared preferences
  final prefs = await SharedPreferences.getInstance();
  prefs.setString('email', email);
  prefs.setString('password', password);

  // Making API call
  var jsonData = await requestPOST("/app/login", {
    'email': email,
    'password': password,
  });

  if (jsonData['error'] == false) {
    // Saving user information to global user variable and toggling isLogged variable
    User user = User.fromMap(jsonData['user']);
    await setGlobalUser(user, jsonData['token']);
    bool userExists = await UserDBProvider.db.userExists(user.email);

    // If the user is not in the db, we add it
    if (!userExists) {
      await UserDBProvider.db.newUser(user);
    }

    // Syncing events for user
    await syncEvents(context);

    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (BuildContext context) => Home()),
      ModalRoute.withName('/'),
    );

    // Determining panel based on user type
    // if (user.type == 'admin') {
    //   // Navigator.pushNamed(context, '/dashboard/admin');
    //   Navigator.pushNamed(context, '/my_tickets');
    // } else if (user.type == 'client' || user.type == 'temporary') {
    //   // Navigator.pushNamed(context, '/dashboard/client');
    //   Navigator.pushNamed(context, '/my_tickets');
    // } else if (user.type == 'ticket_office') {
    //   // Navigator.pushNamed(context, '/access_dash');
    //   Navigator.pushNamed(context, '/my_tickets');
    // } else if (user.type == 'access') {
    //   // Navigator.pushNamed(context, '/access_dash');
    //   Navigator.pushNamed(context, '/my_tickets');
    // } else {
    //   Navigator.pushNamed(context, '/my_tickets');
    // }
  } else {
    // Logging out user (in case it existed)
    await logoutGlobalUser();

    // Sending alert
    Alert(
      context: context,
      type: AlertType.error,
      title: "Error",
      desc: jsonData['error_message'],
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
  }
}
