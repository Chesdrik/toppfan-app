part of api_library;

Future<List<News>> getNews(context) async {
  print("Fetching news");
  // Fetching user from local storage

  // Making API call
  var jsonData = await requestPOST("/app/news", {});

  print("Data fetched");

  // If there is no error, we process data
  List<News> newsList = [];

  if (jsonData == null) {
    print('Offline!');
    return null;
  } else if (jsonData['error'] == false) {
    print("No errors");

    // Parsing news
    for (var jsonNews in jsonData['news']) {
      newsList.add(News.fromMap(jsonNews));
    }
  } else {
    // Error fetching tickets
    Alert(
      context: context,
      type: AlertType.error,
      title: "Error",
      desc: jsonData['error_message'],
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
  }

  // Returning function
  return newsList;
}
