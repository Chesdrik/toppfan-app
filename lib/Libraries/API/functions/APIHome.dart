part of api_library;

Future<void> getHome(context) async {
  // Making API call
  var jsonData = await requestPOST("/app/home", {});
  print(jsonData);

  if (jsonData == null) {
    print('Offline!');
  } else if (jsonData['error'] == false) {
    globalImageLogo = jsonData['logo'];
    globalTitle = jsonData['title'];
    globalPrivacy = jsonData['privacy'];
  } else {
    // Error fetching tickets
    Alert(
      context: context,
      type: AlertType.error,
      title: "Error",
      desc: 'Hay problemas de conexión con el servidor',
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
  }
}
