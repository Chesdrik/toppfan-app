library api_library;

// Required imports
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

// Libraries and Models
import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';
import 'package:toppfan_app/Libraries/DB/DBLibrary.dart';
import 'package:toppfan_app/Models/User.dart';
import 'package:toppfan_app/Models/Ticket.dart';
import 'package:toppfan_app/Models/Event.dart';
import 'package:toppfan_app/Models/Venue.dart';
import 'package:toppfan_app/Models/News.dart';
import 'package:toppfan_app/Views/Home.dart';

// Parts
part 'package:toppfan_app/Libraries/API/functions/APIFunctions.dart';
part 'package:toppfan_app/Libraries/API/functions/APIHome.dart';
part 'package:toppfan_app/Libraries/API/functions/APIPrivacy.dart';
part 'package:toppfan_app/Libraries/API/functions/APILogin.dart';
part 'package:toppfan_app/Libraries/API/functions/APIEvents.dart';
part 'package:toppfan_app/Libraries/API/functions/APITickets.dart';
part 'package:toppfan_app/Libraries/API/functions/APINews.dart';
