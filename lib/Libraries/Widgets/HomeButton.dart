part of widget_library;

class HomeButton extends StatefulWidget {
  String title;
  String action;
  Map<String, dynamic> parameters;
  IconData icon;
  String type = 'text';
  Color backgroundColor = primaryColor;
  Color textColor = lightGray;
  double iconSize = 30;
  int fadeInDelay = 1;

  // Regular home button
  HomeButton({
    this.title,
    this.action,
    this.fadeInDelay,
  }) {
    this.type = 'text';
  }

  HomeButton.icon({
    this.type,
    this.icon,
    this.action,
    this.backgroundColor,
    this.textColor,
    this.iconSize,
    this.fadeInDelay,
  }) {
    this.title = '';
  }

  HomeButton.icontext({
    this.type,
    this.icon,
    this.action,
    this.parameters,
    this.backgroundColor,
    this.textColor,
    this.iconSize,
    this.fadeInDelay,
    this.title = '',
  });

  @override
  _HomeButtonState createState() => _HomeButtonState();
}

class _HomeButtonState extends State<HomeButton> {
  bool _visible = true;

  // @override
  // void initState() {
  //   super.initState();
  //   // print('setting innit state to ' + _visible.toString());
  //   // fadeIn();
  // }

  void fadeIn() {
    print('Fading in');
    var time = widget.fadeInDelay * 75;
    Future.delayed(Duration(milliseconds: time), () {
      print('delay for ' + time.toString());
      setState(() {
        _visible = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
        child: AnimatedOpacity(
          // If the widget is visible, animate to 0.0 (invisible).
          // If the widget is hidden, animate to 1.0 (fully visible).
          opacity: _visible ? 1.0 : 0.0,
          duration: Duration(milliseconds: 800),
          // The green box must be a child of the AnimatedOpacity widget.
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: this.widget.backgroundColor.withOpacity(0.90),
              elevation: 5.0,
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5))),
            ),
            child: Container(
              width: double.infinity,
              height: 140,
              child: Column(children: [
                Spacer(),
                (() {
                  if (this.widget.type == 'text') {
                    return Text(
                      this.widget.title,
                      style: TextStyle(
                        color: this.widget.textColor,
                        fontSize: 20,
                        letterSpacing: 3,
                      ),
                    );
                  } else if (widget.type == 'icon') {
                    return Icon(
                      this.widget.icon,
                      color: this.widget.textColor,
                      size: widget.iconSize,
                    );
                  } else {
                    return Column(
                      children: [
                        Icon(
                          this.widget.icon,
                          color: this.widget.textColor,
                          size: widget.iconSize,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          this.widget.title,
                          style: TextStyle(
                            color: this.widget.textColor,
                            fontSize: 15,
                            letterSpacing: 3,
                          ),
                        ),
                      ],
                    );
                  }
                }()),
                Spacer(),
              ]),
            ),
            onPressed: (() {
              print('Arguments: ');
              print(this.widget.parameters);

              Navigator.pushNamed(context, this.widget.action,
                  arguments: this.widget.parameters);
            }),
          ),
        ));
  }
}
