part of widget_library;

Widget toppfanAppbar(BuildContext context, String appBarTitle,
    String currentPage, String popType) {
  return AppBar(
    // automaticallyImplyLeading: false,
    backgroundColor: lightGray,
    elevation: 0,
    leading: (currentPage != 'home'
        ? IconButton(
            icon: new Icon(Icons.arrow_back),
            color: darkerGray,
            onPressed: () {
              if (popType == 'pop') {
                Navigator.pop(context);
              } else {
                Navigator.popUntil(context, ModalRoute.withName(popType));
              }
            },
          )
        : SizedBox(
            width: 0,
          )),
    centerTitle: true,
    title: Text(
      appBarTitle,
      style: TextStyle(
        color: darkerGray,
      ),
    ),
    actions: [
      (globalUser == null && currentPage != 'privacy'
          ? IconButton(
              onPressed: () {
                Navigator.pushNamed(context, '/privacy');
              },
              icon: Icon(
                Icons.privacy_tip_outlined,
                color: darkerGray,
              ),
            )
          : SizedBox(
              width: 0,
            )),
      (globalUser != null
          ? IconButton(
              onPressed: () {
                Navigator.pushNamed(context, '/account');
              },
              icon: Icon(
                Icons.account_circle,
                color: darkerGray,
              ),
            )
          : IconButton(
              onPressed: () {
                Navigator.pushNamed(context, '/login');
              },
              icon: Icon(
                Icons.login_rounded,
                color: darkerGray,
              ),
            )),
    ],
  );
}
