library widget_library;

// Required imports
import 'package:flutter/material.dart';
import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';
// import 'package:toppfan_app/Libraries/Helpers/HelpersLibrary.dart';

// import 'package:toppfan_app/Views/HTMLView.dart';

// Parts
part 'package:toppfan_app/Libraries/Widgets/HomeButton.dart';
part 'package:toppfan_app/Libraries/Widgets/EndDrawer.dart';
part 'package:toppfan_app/Libraries/Widgets/Appbar.dart';
