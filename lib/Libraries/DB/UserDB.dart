part of db_library;

class UserDBProvider {
  UserDBProvider._();

  static final UserDBProvider db = UserDBProvider._();

  Database _database;
  int _databaseVersion = 1;
  String _databaseName = "toppfanAppUsers.db";
  String _databaseStructure = "CREATE TABLE Users ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "name TEXT,"
      "email TEXT,"
      "type TEXT,"
      "active Text"
      ")";

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Users");
      await db.execute(_databaseStructure);
    }
  }

  newUser(User newUser) async {
    final db = await database;
    // Inserting user in DB
    var insertQuery = await db.rawInsert(
        "INSERT INTO Users (name, email, type, active)"
        " VALUES (?,?,?,?)",
        [
          newUser.name,
          newUser.email,
          newUser.type,
          newUser.active,
        ]);

    return insertQuery;
  }

  blockOrUnblock(User user) async {
    final db = await database;
    User blocked = User(
        id: user.id,
        name: user.name,
        email: user.email,
        type: user.type,
        active: user.active);
    var res = await db.update("Users", blocked.toMap(),
        where: "id = ?", whereArgs: [user.id]);
    return res;
  }

  updateUser(User newUser) async {
    final db = await database;
    var res = await db.update("Users", newUser.toMap(),
        where: "id = ?", whereArgs: [newUser.id]);
    return res;
  }

  getUser(int id) async {
    final db = await database;
    var res = await db.query("Users", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? User.fromMap(res.first) : null;
  }

  getUserEmail(String email) async {
    final db = await database;
    var res = await db.query("Users", where: "email = ?", whereArgs: [email]);
    return res.isNotEmpty ? true : false;
  }

  userExists(String email) async {
    final db = await database;
    var res = await db.query("Users", where: "email = ?", whereArgs: [email]);
    return res.isNotEmpty ? true : false;
  }

  Future<List<User>> getBlockedUsers() async {
    final db = await database;

    print("works");
    // var res = await db.rawQuery("SELECT * FROM User WHERE blocked=1");
    var res = await db.query("Users", where: "blocked = ? ", whereArgs: [1]);

    List<User> list =
        res.isNotEmpty ? res.map((c) => User.fromMap(c)).toList() : [];
    return list;
  }

  Future<List<User>> getAllUsers() async {
    final db = await database;
    var res = await db.query("Users");
    List<User> list =
        res.isNotEmpty ? res.map((c) => User.fromMap(c)).toList() : [];
    return list;
  }

  deleteUser(int id) async {
    final db = await database;
    return db.delete("Users", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("DELETE FROM Users");
  }
}
