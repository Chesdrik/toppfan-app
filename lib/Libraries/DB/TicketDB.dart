part of db_library;

class TicketDBrovider {
  TicketDBrovider._();

  static final TicketDBrovider db = TicketDBrovider._();

  Database _database;
  int _databaseVersion = 33;
  String _databaseName = "toppfanAppTickets.db";
  String _databaseStructure = "CREATE TABLE IF NOT EXISTS Tickets ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "eventId INTEGER,"
      "amount INTEGER,"
      "ticketTypeName TEXT,"
      "ticketTypeColor TEXT,"
      "ticketTypeAccess TEXT,"
      "qr TEXT"
      ");";

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(
      path,
      version: _databaseVersion,
      onOpen: (db) {},
      onCreate: _onCreate,
      onUpgrade: _onUpgrade,
    );
  }

  // _onCreate Script
  Future<void> _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Tickets");
      await db.execute(_databaseStructure);
    }
  }

  Future<void> newTicket(Ticket newTicket) async {
    final db = await database;
    // Inserting Ticket in DB
    var insertQuery = await db.rawInsert(
        "INSERT INTO Tickets (id, eventId, amount, ticketTypeName, ticketTypeColor, ticketTypeAccess, qr)"
        " VALUES (?,?,?,?,?,?,?)",
        [
          newTicket.id,
          newTicket.eventId,
          newTicket.amount,
          newTicket.ticketTypeName,
          newTicket.ticketTypeColor,
          newTicket.ticketTypeAccess,
          newTicket.qr,
        ]);

    return insertQuery;
  }

  Future<void> updateTicket(Ticket newTicket) async {
    final db = await database;
    var res = await db.update("Tickets", newTicket.toMap(),
        where: "id = ?", whereArgs: [newTicket.id]);
    return res;
  }

  Future<void> getTicket(int id) async {
    final db = await database;
    var res = await db.query("Tickets", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Ticket.fromMap(res.first) : null;
  }

  Future<List<Ticket>> getTicketsForEvent(int id) async {
    print("DEBUG: Fetching tickets for eventId = " + id.toString());
    final db = await database;
    var res = await db.query("Tickets", where: "eventId = ?", whereArgs: [id]);

    List<Ticket> tickets = [];
    if (res.isNotEmpty) {
      // print('Tickets found');
      // Adding tickets to list
      for (var ticket in res) {
        // print('Adding Ticket');
        // print(ticket.toString());
        // print(Ticket.fromMap(ticket));
        tickets.add(new Ticket.fromMap(ticket));
        // print('added!');
      }
    }

    // Returning data
    return tickets;
  }

  Future<bool> hasTicketsForEvent(int eventId) async {
    final db = await database;
    var res = await db
        .rawQuery("SELECT COUNT(*) FROM Tickets WHERE eventId=?", [eventId]);
    var total = Sqflite.firstIntValue(res);

    return (total > 0 ? true : false);
  }

  Future<void> ticketExists(String id) async {
    final db = await database;
    var res = await db.query("Tickets", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? true : false;
  }

  Future<List<Ticket>> getAllTickets() async {
    final db = await database;
    var res = await db.query("Tickets");
    List<Ticket> list =
        res.isNotEmpty ? res.map((c) => Ticket.fromMap(c)).toList() : [];
    return list;
  }

  Future<void> deleteTicket(int id) async {
    final db = await database;
    return db.delete("Tickets", where: "id = ?", whereArgs: [id]);
  }

  Future<void> deleteAll() async {
    final db = await database;
    db.rawDelete("DELETE FROM Tickets;");
  }

  Future<List<int>> eventIdsWithTickets() async {
    final db = await database;
    var res = await db.rawQuery("SELECT DISTINCT eventId FROM Tickets;");

    // Parsing each row
    List<int> events = [];
    if (res.isNotEmpty) {
      for (var ev in res) {
        events.add(ev['eventId']);
      }
    }

    // Returning data
    return events;
  }
}
