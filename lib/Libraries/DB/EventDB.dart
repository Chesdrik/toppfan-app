part of db_library;

class EventDBrovider {
  EventDBrovider._();

  static final EventDBrovider db = EventDBrovider._();

  Database _database;
  int _databaseVersion = 2;
  String _databaseName = "toppfanAppEvents.db";
  String _databaseStructure = "CREATE TABLE Events ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "name TEXT,"
      "date TEXT,"
      "venueId INTEGER,"
      "img TEXT"
      ")";

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  Future<void> _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Events");
      await db.execute(_databaseStructure);
    }
  }

  Future<void> newEvent(Event newEvent) async {
    final db = await database;
    // Inserting Event in DB
    var insertQuery = await db.rawInsert(
        "INSERT INTO Events (id, name, date, venueId, img)"
        " VALUES (?,?,?,?,?)",
        [
          newEvent.id,
          newEvent.name,
          newEvent.date,
          newEvent.venueId,
          newEvent.img,
        ]);

    return insertQuery;
  }

  Future<void> updateEvent(Event newEvent) async {
    final db = await database;
    var res = await db.update("Events", newEvent.toMap(),
        where: "id = ?", whereArgs: [newEvent.id]);
    return res;
  }

  Future<Event> getEvent(int id) async {
    final db = await database;
    var res = await db.query("Events", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Event.fromMap(res.first) : null;
  }

  Future<bool> eventExists(String id) async {
    final db = await database;
    var res = await db.query("Events", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? true : false;
  }

  Future<List<Event>> getAllEvents() async {
    final db = await database;
    var res = await db.query("Events");
    List<Event> list =
        res.isNotEmpty ? res.map((c) => Event.fromMap(c)).toList() : [];
    return list;
  }

  Future<void> deleteEvent(int id) async {
    final db = await database;
    return db.delete("Events", where: "id = ?", whereArgs: [id]);
  }

  Future<void> deleteAll() async {
    final db = await database;
    db.rawDelete("DELETE FROM Events;");
  }

  Future<List<Event>> eventsWithTickets() async {
    List<int> eventIds = await TicketDBrovider.db.eventIdsWithTickets();

    // Parsing each row
    List<Event> events = [];
    if (eventIds.isNotEmpty) {
      for (var ev in eventIds) {
        Event event = await EventDBrovider.db.getEvent(ev);

        events.add(event);
      }
    }

    // Returning data
    return events;
  }
}
