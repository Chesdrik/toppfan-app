part of db_library;

class VenueDBrovider {
  VenueDBrovider._();

  static final VenueDBrovider db = VenueDBrovider._();

  Database _database;
  int _databaseVersion = 2;
  String _databaseName = "toppfanAppVenues.db";
  String _databaseStructure = "CREATE TABLE IF NOT EXISTS Venues ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "name TEXT,"
      "lat DECIMAL(8,6),"
      "long DECIMAL(9,6),"
      "img TEXT"
      ");";

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // String path = join(await getDatabasesPath(), _databaseName);
    print("DEBUG: initDB for Venues");

    return await openDatabase(
      path,
      version: _databaseVersion,
      onOpen: (db) {},
      onCreate: _onCreate,
      onUpgrade: _onUpgrade,
    );
  }

  // _onCreate Script
  Future<void> _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Venues;");
      await db.execute(_databaseStructure);
    }
  }

  Future<void> newVenue(Venue newVenue) async {
    final db = await database;
    // Inserting Venue in DB
    var insertQuery = await db.rawInsert(
        "INSERT INTO Venues (id, name, lat, long, img)"
        " VALUES (?,?,?,?,?)",
        [
          newVenue.id,
          newVenue.name,
          newVenue.lat,
          newVenue.long,
          newVenue.img,
        ]);

    return insertQuery;
  }

  Future<void> updateVenue(Venue newVenue) async {
    final db = await database;
    var res = await db.update("Venues", newVenue.toMap(),
        where: "id = ?", whereArgs: [newVenue.id]);
    return res;
  }

  Future<Venue> getVenue(int id) async {
    final db = await database;
    var res = await db.query("Venues", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Venue.fromMap(res.first) : null;
  }

  Future<bool> venueExists(int id) async {
    final db = await database;
    var res = await db.query("Venues", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? true : false;
  }

  Future<String> getVenueName(int id) async {
    String venueName = "Sede por definir";

    if (await VenueDBrovider.db.venueExists(id.toInt())) {
      Venue venue = await VenueDBrovider.db.getVenue(id.toInt());
      venueName = venue.name;
    }

    return venueName;
  }

  Future<List<Venue>> getAllVenues() async {
    final db = await database;
    var res = await db.query("Venues");
    List<Venue> list =
        res.isNotEmpty ? res.map((c) => Venue.fromMap(c)).toList() : [];
    return list;
  }

  Future<void> deleteVenue(int id) async {
    final db = await database;
    return db.delete("Venues", where: "id = ?", whereArgs: [id]);
  }

  Future<void> deleteAll() async {
    final db = await database;
    db.rawDelete("DELETE FROM Venues;");
  }
}
