part of app_styles;

InputDecoration inputDecorationStyle(String hintText, IconData prefixIcon) {
  return InputDecoration(
    isDense: true,
    fillColor: Colors.white,
    prefixIcon: Icon(prefixIcon, color: Colors.black45),
    hintText: hintText,
    filled: true,
    hintStyle: TextStyle(color: Colors.black87, fontSize: 10.0),
    enabledBorder: const OutlineInputBorder(
      borderRadius: const BorderRadius.all(const Radius.circular(5.0)),
      borderSide: const BorderSide(color: Colors.black12, width: 1.5),
    ),
    border: new OutlineInputBorder(
      borderRadius: const BorderRadius.all(const Radius.circular(5.0)),
      borderSide: const BorderSide(color: Colors.black38, width: 1.5),
    ),
    focusedBorder: new OutlineInputBorder(
      borderRadius: const BorderRadius.all(const Radius.circular(5.0)),
      borderSide: const BorderSide(color: Colors.black38, width: 1.5),
    ),
  );
}
