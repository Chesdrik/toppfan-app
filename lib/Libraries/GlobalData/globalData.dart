library global_data;

// Required imports
import 'package:flutter/material.dart';
import 'package:toppfan_app/Models/User.dart';
import 'package:toppfan_app/Libraries/Helpers/HelpersLibrary.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

// Parts
part 'package:toppfan_app/Libraries/GlobalData/globals.dart';
part 'package:toppfan_app/Libraries/GlobalData/globalURLs.dart';
