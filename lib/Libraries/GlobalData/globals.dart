part of global_data;

// Colors
Color primaryColor = HexColor('#374564');
Color secondaryColor = HexColor('#4CB9E4');

// https://www.materialpalette.com/colors
Color lightGray = HexColor('#f5f5f5'); // 100
Color mediumGray = HexColor('#eeeeee'); // 200
Color darkGray = HexColor('#9e9e9e'); // 500
Color darkerGray = HexColor('#616161'); // 700

// Session variables
String authKey;
User globalUser;
bool isLogged;
String globalImageLogo;
String globalTitle = "Toppfan";
String globalPrivacy = "";

// HTTPS
bool useHttps = true;

Future<void> initGlobalData() async {
  globalUser = await getGlobalGlobalUser();
  // print("Setting global user");
  // print(globalUser.toString());
}

Future<void> setGlobalUser(User user, String token) async {
  // Instance shared preferences
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  // print("Setting global user with token " + token);
  // print("DEBUG: " + json.encode(user.toMap()));

  // Saving user info y SharedPreferences for persistence
  await prefs.setString('globalUser', json.encode(user.toMap()));
  await prefs.setBool('isLogged', true);
  await prefs.setString('authKey', token);

  // Setting global variables
  globalUser = user;
  isLogged = true;
  authKey = token;
}

Future<void> logoutGlobalUser() async {
  // Instance shared preferences
  final SharedPreferences prefs = await SharedPreferences.getInstance();

  // Setting isLogged to false and removing globalUser and authKey
  await prefs.setBool('isLogged', false);
  await prefs.remove('globalUser');
  await prefs.remove('authKey');

  // Setting global variables
  globalUser = null;
  isLogged = false;
  authKey = null;
}

Future<User> getGlobalGlobalUser() async {
  // Instance shared preferences
  final SharedPreferences prefs = await SharedPreferences.getInstance();

  // Saving user info y SharedPreferences for persistence
  String userString = prefs.getString('globalUser');

  isLogged = (userString == null ? false : true);

  return (userString == null ? null : User.fromMap(json.decode(userString)));
}

Future<bool> getGlobalIsLogged() async {
  // Instance shared preferences
  final SharedPreferences prefs = await SharedPreferences.getInstance();

  // Returning isLogged
  return prefs.getBool('isLogged');
}

Future<String> getGlobalAuthKey() async {
  // Instance shared preferences
  final SharedPreferences prefs = await SharedPreferences.getInstance();

  // Returning
  return prefs.getString('authKey');
}
