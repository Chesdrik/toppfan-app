part of helpers;

class FadeTransitionWidget extends StatefulWidget {
  final String _assetURL;

  FadeTransitionWidget(this._assetURL);

  @override
  _FadeTransitionWidgetState createState() => _FadeTransitionWidgetState();
}

class _FadeTransitionWidgetState extends State<FadeTransitionWidget>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  CurvedAnimation _curve;

  @override
  void initState() {
    ///An animation controller lets you control the
    ///duration of an animation
    ///Here the ticker for vsync provider is provided
    ///by the SingleTickerProviderStateMixin

    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );

    ///Providing our animation with a curve (Parent here is the controller
    ///above)
    _curve = CurvedAnimation(parent: _controller, curve: Curves.easeIn);

    ///Creating a Tween animation with start and end values for the
    ///opacity and providing it with our animation controller
    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_curve);

    ///Set a status listener to our animation to control the behaviour
    ///of our animation
    // _animation.addStatusListener((status) {
    // if (status == AnimationStatus.completed)
    //   _controller.reverse();
    // else if (status == AnimationStatus.dismissed) _controller.forward();
    // });

    _controller.forward();

    super.initState();
  }

  @override
  void dispose() {
    ///Don't forget to clean up resources when you are done using it
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FadeTransition(
        // Providing our animation to opacity property
        opacity: _animation,
        child: Image.asset(widget._assetURL),
      ),
    );
  }
}
