part of helpers;

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: lightGray,
      child: SpinKitThreeBounce(
        color: primaryColor,
        size: 30.0,
      ),
    );
  }
}

class ContainedLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SpinKitThreeBounce(
        color: primaryColor,
        size: 30.0,
      ),
    );
  }
}
