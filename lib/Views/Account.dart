import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:sizer/sizer.dart';

// import 'package:cached_network_image/cached_network_image.dart';
import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';
import 'package:toppfan_app/Libraries/Widgets/WidgetLibrary.dart';

class Account extends StatefulWidget {
  @override
  _AccountState createState() => new _AccountState();
}

class _AccountState extends State<Account> {
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      appBar: toppfanAppbar(context, "Mi cuenta", 'account', 'pop'),
      backgroundColor: lightGray,
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Center(
          child: Column(
            children: <Widget>[
              Spacer(flex: 4),
              // Icon(Icons.verified_user_outlined),
              Icon(
                Icons.supervised_user_circle_outlined,
                color: darkerGray,
                size: 80.0,
              ),
              Spacer(),
              Divider(),
              Spacer(),
              Text(
                globalUser.name,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: darkerGray,
                ),
              ),
              Spacer(),
              Text(
                globalUser.email,
                style: TextStyle(
                  fontSize: 15,
                  color: darkerGray,
                ),
              ),
              Spacer(flex: 3),
              ElevatedButton(
                onPressed: () async {
                  print("Aviso de privacidad");
                  // Pushing view
                  Navigator.pushNamed(context, '/privacy');
                },
                child: Text("Aviso de privacidad"),
                style: ElevatedButton.styleFrom(
                  primary: darkGray.withOpacity(0.90),
                  elevation: 5.0,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                ),
              ),
              Spacer(),
              ElevatedButton(
                onPressed: () async {
                  print("Logout");
                  await logoutGlobalUser();

                  // Pushing view
                  Navigator.pushNamed(context, '/');
                },
                child: Text("Cerrar sesión"),
                style: ElevatedButton.styleFrom(
                  primary: darkGray.withOpacity(0.90),
                  elevation: 5.0,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                ),
              ),
              Spacer(flex: 4),
            ],
          ),
        ),
      ),
    );
  }
}
