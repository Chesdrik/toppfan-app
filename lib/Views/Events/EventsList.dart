import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';
import 'package:toppfan_app/Libraries/Helpers/HelpersLibrary.dart';
import 'package:toppfan_app/Libraries/API/apiLibrary.dart';
import 'package:toppfan_app/Libraries/DB/DBLibrary.dart';
import 'package:toppfan_app/Libraries/Widgets/WidgetLibrary.dart';
// import 'package:toppfan_app/Models/Venue.dart';

class EventsList extends StatefulWidget {
  static const routeName = '/events';

  @override
  _EventsListState createState() => _EventsListState();
}

class _EventsListState extends State<EventsList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: toppfanAppbar(context, "Eventos", 'eventList', 'pop'),
      body: FutureBuilder<List>(
        future: EventDBrovider.db.getAllEvents(),
        builder: (context, events) {
          return !events.hasData
              ? Center(
                  child: Loading(),
                )
              : RefreshIndicator(
                  onRefresh: () async {
                    // Syncing events
                    await syncEvents(context); // Fetching events from API

                    // Updating app state with returned data
                    setState(() {});
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListView.builder(
                      itemCount: events.data.length,
                      itemBuilder: (_, int position) {
                        final item = events.data[position];
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 24.0),
                          child: Card(
                            child: Column(
                              children: [
                                Container(
                                  child: CachedNetworkImage(
                                    imageUrl: item.img,
                                    placeholder: (context, url) => SizedBox(
                                      child: CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                darkGray),
                                      ),
                                      height: 50.0,
                                      width: 50.0,
                                    ),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 16.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        item.name,
                                        style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      FutureBuilder(
                                        future: VenueDBrovider.db
                                            .getVenueName(item.venueId),
                                        initialData: "Cargando sede..",
                                        builder: (BuildContext context,
                                            AsyncSnapshot<String> text) {
                                          return new Container(
                                            padding:
                                                new EdgeInsets.only(top: 8.0),
                                            child: new Text(
                                              text.data,
                                              style: new TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                      Text(
                                        item.date,
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14.0,
                                        ),
                                      ),
                                      SizedBox(height: 5.0),
                                      FutureBuilder(
                                        future: TicketDBrovider.db
                                            .hasTicketsForEvent(item.id),
                                        initialData: false,
                                        builder:
                                            (BuildContext context, snapshot) {
                                          if (snapshot.connectionState ==
                                              ConnectionState.done) {
                                            if (snapshot.data) {
                                              return Container(
                                                height: 130,
                                                child: HomeButton.icontext(
                                                  type: 'icontext',
                                                  icon: Icons.qr_code_2_rounded,
                                                  title: 'Mis boletos',
                                                  action: '/events/tickets',
                                                  parameters: {
                                                    'event': item,
                                                  },
                                                  backgroundColor: lightGray,
                                                  textColor: darkerGray,
                                                  iconSize: 30,
                                                  fadeInDelay: 1,
                                                ),
                                              );
                                            } else {
                                              return SizedBox(
                                                height: 0,
                                              );
                                            }
                                          } else if (snapshot.connectionState ==
                                              ConnectionState.waiting) {
                                            return SizedBox(
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    new AlwaysStoppedAnimation<
                                                        Color>(darkGray),
                                              ),
                                              height: 50.0,
                                              width: 50.0,
                                            );
                                            // return SizedBox(height: 0);
                                          } else {
                                            return SizedBox(height: 0);
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                );
        },
      ),
    );
  }
}
