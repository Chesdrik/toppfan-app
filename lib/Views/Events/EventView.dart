import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:io' show Platform;
// import 'package:ticket_pass_package/ticket_pass.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:scroll_snap_list/scroll_snap_list.dart';

import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';
// import 'package:toppfan_app/Libraries/API/apiLibrary.dart';
import 'package:toppfan_app/Libraries/Helpers/HelpersLibrary.dart';
import 'package:toppfan_app/Libraries/Widgets/WidgetLibrary.dart';
import 'package:toppfan_app/Libraries/DB/DBLibrary.dart';
// import 'package:toppfan_app/Models/Venue.dart';
// import 'package:toppfan_app/Models/Event.dart';
import 'package:toppfan_app/Models/Ticket.dart';

// import 'package:flutter/services.dart';
// import 'dart:convert';
// import 'package:toppfan_app/Libraries/Helpers/HelpersLibrary.dart';
// import 'package:toppfan_app/Libraries/API/apiLibrary.dart';
// import 'package:toppfan_app/Models/OrderDetails.dart';
// import 'package:toppfan_app/Views/UI.dart';
// import 'package:esys_flutter_share/esys_flutter_share.dart';
// import 'package:add_2_calendar/add_2_calendar.dart' as EVCalendar;
// class EventViewArguments {
//   final Ticket ticket;
//   EventViewArguments(this.ticket);
// }

class EventView extends StatefulWidget {
  static const routeName = '/events/tickets';

  @override
  _EventViewState createState() => _EventViewState();
}

class _EventViewState extends State<EventView> {
  // final args = ModalRoute.of(context).settings.arguments;
  // print()
  // final TicketArguments args = ModalRoute.of(context).settings.arguments;
  // final Ticket ticket = ModalRoute.of(context).settings.arguments;

  String title = "Evento";
  int focusedIndex = 0;

  Future<void> secureScreen(String actionType) async {
    if (actionType == 'up') {
      await FlutterWindowManager.addFlags(FlutterWindowManager.FLAG_SECURE);
    } else {
      await FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
    }
  }

  @override
  void initState() {
    if (Platform.isAndroid) {
      secureScreen('up');
    }
    super.initState();
  }

  @override
  void dispose() {
    if (Platform.isAndroid) {
      secureScreen('down');
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;

    return Scaffold(
      appBar: toppfanAppbar(context, "Evento", 'event', '/my_tickets'),
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints:
              BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 50.h,
                child: Column(
                  children: [
                    eventImage(args),
                    eventName(args['event'].name),
                    eventVenue(args['event'].venueId),
                    eventDate(args['event'].date),
                  ],
                ),
              ),
              Expanded(
                child: ticketsWidget(args),
              ),
            ],
          ),
        ),
      ),
    );
  }

  FutureBuilder<List<Ticket>> ticketsWidget(args) {
    return FutureBuilder<List<Ticket>>(
      future: TicketDBrovider.db.getTicketsForEvent(args['event'].id),
      builder: (context, snapshot) {
        List<Ticket> tickets = snapshot.data ?? [];

        return ScrollSnapList(
          onItemFocus: _onItemFocus,
          itemSize: 100.w,
          itemCount: tickets.length,
          // dynamicItemSize: true,
          itemBuilder: (context, index) {
            return new Container(
              width: 100.w,
              height: 100.h,
              child: Column(
                children: [
                  Text(
                    "Zona " + tickets[index].ticketTypeName,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Divider(
                    color: HexColor("#" + tickets[index].ticketTypeColor),
                    height: 10,
                    thickness: 3,
                    indent: 20,
                    endIndent: 20,
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    height: 30.h,
                    width: 60.w,
                    child: SvgPicture.string(tickets[index].qr),
                  ),
                  Text(
                    "Ingreso por accesos",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    tickets[index].ticketTypeAccess,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Boleto " +
                          (index + 1).toString() +
                          " de " +
                          tickets.length.toString(),
                      style: TextStyle(
                        fontSize: 12,
                        color: darkerGray,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  Widget eventVenue(venue) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: FutureBuilder(
        future: VenueDBrovider.db.getVenueName(venue),
        initialData: "Cargando sede..",
        builder: (BuildContext context, AsyncSnapshot<String> text) {
          return new Container(
            padding: new EdgeInsets.only(top: 4.0),
            child: new Text(
              text.data,
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
          );
        },
      ),
    );
  }

  Widget eventDate(date) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        date,
        style: new TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 14.0,
        ),
      ),
    );
  }

  Widget eventName(String name) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Text(
        name,
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget eventImage(args) {
    return Container(
      child: CachedNetworkImage(
        imageUrl: args['event'].img,
        placeholder: (context, url) => SizedBox(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(darkGray),
          ),
          height: 50.0,
          width: 50.0,
        ),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    );
  }

  void _onItemFocus(int index) {
    print("Focus: " + index.toString());
    setState(() {
      focusedIndex = index;
    });
  }

  Future<SvgPicture> displayQR(qrContent) async {
    // final DrawableRoot svgRoot = await svg.fromSvgString(qrContent, qrContent);
    // var svgimage = await svgRoot.toPicture().toImage(500, 500);

    return SvgPicture.string(qrContent);
  }
}
