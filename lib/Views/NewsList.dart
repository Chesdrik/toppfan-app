import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:sizer/sizer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';
import 'package:toppfan_app/Libraries/Helpers/HelpersLibrary.dart';
import 'package:toppfan_app/Libraries/Widgets/WidgetLibrary.dart';
import 'package:toppfan_app/Libraries/API/apiLibrary.dart';
import 'package:toppfan_app/Models/News.dart';

class NewsList extends StatefulWidget {
  @override
  _NewsListState createState() => new _NewsListState();
}

class _NewsListState extends State<NewsList> {
  bool _offline = false;
  bool _loading = true;
  List<News> _newsList;

  void initState() {
    super.initState();
    syncNews();
  }

  Future<void> syncNews() async {
    print('Fetching news');
    // Setting loading animation
    setState(() {
      _loading = true;
    });

    // Syncing news
    var _response = await getNews(context); // Fetching news from API

    // Updating news if we got anything
    if (_response != null) {
      _newsList = _response;
    }

    // Updating app state with returned data
    setState(() {
      // Defining if we are offline
      print(_newsList);
      _offline = (_newsList == null ? true : false);
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
        appBar: toppfanAppbar(context, "Noticias", 'newsList', 'pop'),
        backgroundColor: lightGray,
        body: RefreshIndicator(
            onRefresh: () async {
              print('Sync again!');
              // Syncing events
              await syncNews(); // Fetching events from API
            },
            child: (_loading
                ? Center(
                    child: Loading(),
                  )
                : (_offline
                    ? Center(
                        child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 18.0, vertical: 18.0),
                            child: ListView.builder(
                                // controller: scrollController,
                                physics: const AlwaysScrollableScrollPhysics(),
                                itemCount: 1,
                                itemBuilder: (context, index) {
                                  return Text(
                                      "Para poder ver las noticias necesitas tener conexión a internet");
                                })
                            // Column(
                            //   children: [
                            //     Spacer(),
                            //     Text(
                            //       "Modo offline",
                            //       style: TextStyle(
                            //         fontSize: 18.0,
                            //       ),
                            //     ),
                            //     Divider(),

                            //     Spacer(),
                            //   ],
                            // ),
                            ),
                      )
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListView.builder(
                          itemCount: _newsList.length,
                          itemBuilder: (_, int position) {
                            final item = _newsList[position];
                            return Padding(
                              padding: const EdgeInsets.only(bottom: 24.0),
                              child: Card(
                                child: Column(
                                  children: [
                                    Container(
                                      width: double.infinity,
                                      child: CachedNetworkImage(
                                        fit: BoxFit.fitWidth,
                                        imageUrl: item.img,
                                        placeholder: (context, url) => Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Center(
                                            child: SizedBox(
                                              height: 50.0,
                                              width: 50.0,
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    new AlwaysStoppedAnimation<
                                                        Color>(darkGray),
                                              ),
                                            ),
                                          ),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                      ),
                                    ),
                                    Container(
                                      width: double.infinity,
                                      padding: EdgeInsets.symmetric(
                                          vertical: 8.0, horizontal: 8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            item.title,
                                            style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Container(
                                            padding:
                                                new EdgeInsets.only(top: 8.0),
                                            child: new Text(
                                              item.date,
                                              style: new TextStyle(
                                                // fontWeight: FontWeight.bold,
                                                fontSize: 14.0,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            item.content,
                                            style: new TextStyle(
                                              fontSize: 14.0,
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.all(10),
                                          ),
                                          (item.url != ""
                                              ? Row(
                                                  children: [
                                                    Spacer(),
                                                    ElevatedButton.icon(
                                                        onPressed: () {
                                                          print('Go to url');
                                                          launchURL(item.url);
                                                        },
                                                        label: Text("Ver nota"),
                                                        style: ButtonStyle(
                                                          backgroundColor:
                                                              MaterialStateProperty
                                                                  .all<Color>(
                                                                      darkGray),
                                                        ),
                                                        icon: Icon(Icons
                                                            .insert_link_rounded)),
                                                  ],
                                                )
                                              : SizedBox(width: 0)),
                                          SizedBox(height: 5.0),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      )))));
  }

  void launchURL(String url) async {
    await launch(url);
    // if(await canLaunch(url)){
    //   await launch(url);
    // }else{
    //   throw 'Could not launch $url';
    // }
  }
}
