import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';
import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';
import 'package:toppfan_app/Libraries/Helpers/HelpersLibrary.dart';
import 'package:toppfan_app/Libraries/API/apiLibrary.dart';
// import 'package:toppfan_app/Widgets/Login/LoginForm.dart';

class LoadingHome extends StatefulWidget {
  @override
  _LoadingHomeState createState() => new _LoadingHomeState();
}

class _LoadingHomeState extends State<LoadingHome> {
  void initState() {
    super.initState();
    fetchHome();
  }

  Future<void> fetchHome() async {
    // await logoutGlobalUser();

    // Fetching data from server
    await initGlobalData(); // Initialize global data from sahred preferences
    await getHome(context); // Fetching data from API /home service
    await syncEvents(context); // Fetching events from API
    await syncTickets(context); // Fetching events from API

    print('ya paso');

    // Pushing view
    Navigator.pushNamed(context, '/home');
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      backgroundColor: lightGray,
      body: Container(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  height: 15.h,
                  child: Row(
                    children: <Widget>[
                      Spacer(),
                      Loading(),
                      Spacer(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
