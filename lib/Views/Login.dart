import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';

import 'package:toppfan_app/Widgets/Login/LoginForm.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => new _LoginState();
}

class _LoginState extends State<Login> {
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      backgroundColor: lightGray,
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              CachedNetworkImage(
                width:
                    (SizerUtil.deviceType == DeviceType.mobile ? 25.w : 30.w),
                imageUrl: globalImageLogo,
                placeholder: (context, url) => SizedBox(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(darkGray),
                  ),
                  height: 50.0,
                  width: 50.0,
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
              LoginForm(),
            ],
          ),
        ),
      ),
    );
  }
}
