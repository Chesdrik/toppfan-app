import 'package:flutter/material.dart';
import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';
// import 'package:toppfan_app/Libraries/Helpers/HelpersLibrary.dart';
import 'package:toppfan_app/Libraries/Widgets/WidgetLibrary.dart';

// import 'package:toppfan_app/Widgets/CSDrawer.dart';
// import 'package:toppfan_app/Widgets/HomeButton.dart';

class ClientDashboard extends StatelessWidget {
  static const routeName = '/dashboard/admin';

  @override
  Widget build(BuildContext context) {
    return Container(
      color: lightGray,
      child: SafeArea(
        top: false,
        child: Scaffold(
          backgroundColor: lightGray,
          body: SafeArea(
            child: Column(
              children: [
                Spacer(),
                Row(
                  children: <Widget>[
                    HomeButton(
                        title: 'Boletos',
                        action: '/tickets/list',
                        fadeInDelay: 1),
                    HomeButton(
                        title: 'Partidos', action: '/partidos', fadeInDelay: 2),
                  ],
                ),
                Row(
                  children: <Widget>[
                    // HomeButton(
                    //     title: 'Loyalty', action: '/loyalty', fadeInDelay: 3),
                    HomeButton(
                        title: 'Mi cuenta', action: '/account', fadeInDelay: 4),
                  ],
                ),
                Spacer(),
              ],
            ),
          ),
          // endDrawer: EndDrawer(),
          // bottomNavigationBar: AppBottom(),
        ),
      ),
    );
  }
}
