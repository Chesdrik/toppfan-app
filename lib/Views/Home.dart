import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';

import 'package:toppfan_app/Libraries/Widgets/WidgetLibrary.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: toppfanAppbar(context, globalTitle, 'home', ''),
        backgroundColor: lightGray,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                width: 25.w,
                child: CachedNetworkImage(
                  imageUrl: globalImageLogo,
                  placeholder: (context, url) => SizedBox(
                    // child: CircularProgressIndicator(
                    //   valueColor: new AlwaysStoppedAnimation<Color>(darkGray),
                    // ),
                    child: SizedBox(
                      width: 0,
                    ),
                    height: 25.w,
                    width: 25.w,
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
              (isLogged
                  ? Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Text("Bienvenido " + globalUser.name),
                    )
                  : SizedBox(
                      width: 0,
                    )),
              HomeButton.icontext(
                type: 'icontext',
                icon: Icons.qr_code_rounded,
                title: 'Mis boletos',
                action: '/my_tickets',
                backgroundColor: lightGray,
                textColor: darkerGray,
                iconSize: 50,
                fadeInDelay: 1,
              ),
              HomeButton.icontext(
                type: 'icontext',
                icon: Icons.event,
                title: 'Eventos',
                action: '/events',
                backgroundColor: lightGray,
                textColor: darkerGray,
                iconSize: 50,
                fadeInDelay: 1,
              ),
              HomeButton.icontext(
                type: 'icontext',
                icon: Icons.notes_rounded,
                title: 'Noticias',
                action: '/news',
                backgroundColor: lightGray,
                textColor: darkerGray,
                iconSize: 50,
                fadeInDelay: 1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
