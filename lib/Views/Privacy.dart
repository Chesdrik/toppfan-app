import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:sizer/sizer.dart';

// import 'package:cached_network_image/cached_network_image.dart';
import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';
import 'package:toppfan_app/Libraries/API/apiLibrary.dart';
import 'package:toppfan_app/Libraries/Widgets/WidgetLibrary.dart';
import 'package:toppfan_app/Libraries/Helpers/HelpersLibrary.dart';
import 'package:flutter_html/flutter_html.dart';

class Privacy extends StatefulWidget {
  @override
  _PrivacyState createState() => new _PrivacyState();
}

class _PrivacyState extends State<Privacy> {
  void initState() {
    super.initState();
    getPrivacyService(context);
  }

  Future<void> getPrivacyService(context) async {
    print('Fetching privacy wooot');
    await getPrivacy(context);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return Scaffold(
      appBar: toppfanAppbar(context, "Privacidad", 'privacy', 'pop'),
      backgroundColor: lightGray,
      body: (globalPrivacy == null
          ? Loading()
          : SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Html(
                  data: globalPrivacy,
                  // style: {
                  //   "p": Style(
                  //     fontSize: 18,
                  //     fontWeight: FontWeight.bold,
                  //     color: darkerGray,
                  //   ),
                  // },
                  // style: TextStyle(
                  //   fontSize: 18,
                  //   fontWeight: FontWeight.bold,
                  //   color: darkerGray,
                  // ),
                ),
              ),
              // Column(
              //   children: <Widget>[
              //     Icon(
              //       Icons.privacy_tip_outlined,
              //       color: darkerGray,
              //       size: 80.0,
              //     ),
              //     Text(
              //       globalPrivacy,
              //       style: TextStyle(
              //         fontSize: 18,
              //         fontWeight: FontWeight.bold,
              //         color: darkerGray,
              //       ),
              //     ),
              //   ],
              // ),
            )),
    );
  }
}
