import 'package:flutter/material.dart';
import 'dart:async';
import 'package:sizer/sizer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:toppfan_app/Libraries/API/apiLibrary.dart';
import 'package:toppfan_app/Libraries/GlobalData/globalData.dart';
import 'package:toppfan_app/Libraries/Helpers/HelpersLibrary.dart';
import 'package:toppfan_app/Libraries/AppStyles/AppStyles.dart';

// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:toppfan_app/db/DatabaseUser.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:toppfan_app/Views/AdminDashboard.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  String _defaultEmail = '';
  String _defaultPassword = '';
  bool _loadingLogin = false;

  // Fetching shared preferences instance for username and password
  @override
  void initState() {
    super.initState();
    _defaultEmail = "";
    _defaultPassword = "";

    fetchEmailPass();
  }

  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return (_loadingLogin
        ? Container(
            height: (SizerUtil.deviceType == DeviceType.mobile ? 45.h : 40.w),
            width: 40.w,
            child: Column(
              children: <Widget>[
                Spacer(),
                ContainedLoading(),
                Spacer(),
              ],
            ),
          )
        : Container(
            height: (SizerUtil.deviceType == DeviceType.mobile ? 55.h : 40.h),
            width: (SizerUtil.deviceType == DeviceType.mobile ? 80.w : 40.w),
            child: Padding(
                padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 46.0),
                child: Column(
                  children: <Widget>[
                    TextField(
                      controller: emailController,
                      decoration:
                          inputDecorationStyle("Usuario", Icons.email_outlined),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    TextField(
                      controller: passwordController,
                      obscureText: true,
                      decoration: inputDecorationStyle(
                          "Contraseña", Icons.lock_outline),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Center(
                      child: ElevatedButton(
                        child: Text("Iniciar sesión",
                            style: TextStyle(
                                color: HexColor('#FFFFFF'),
                                fontFamily: "Poppins-Bold",
                                fontSize: 15,
                                letterSpacing: 1.0)),
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                          elevation: 5.0,
                          textStyle: TextStyle(color: Colors.white),
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            side: BorderSide(color: primaryColor),
                          ),
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 25),
                        ),
                        onPressed: () async {
                          setState(() {
                            _loadingLogin = true;
                          });

                          await apiLogin(context, emailController.text,
                              passwordController.text);

                          setState(() {
                            _loadingLogin = false;
                          });
                        },
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextButton(
                      child: Text(
                        "¿Olvidaste tu contraseña?",
                        style: TextStyle(
                          color: darkGray,
                          fontFamily: "Poppins-Medium",
                          fontSize: 8.sp,
                        ),
                      ),
                      onPressed: _launchPswdRecovery,
                    ),
                    // TextButton(
                    //   child: Text(
                    //     "Activa tu cuenta",
                    //     style: TextStyle(
                    //       color: darkGray,
                    //       fontFamily: "Poppins-Medium",
                    //       fontSize: 8.sp,
                    //     ),
                    //   ),
                    //   onPressed: _launchPswdRecovery,
                    // ),
                  ],
                ))));
  }

  _launchPswdRecovery() async {
    var url = "https://" + baseURL + "/usuarios/password/reset";

    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<Null> fetchEmailPass() async {
    final prefs = await SharedPreferences.getInstance();
    _defaultEmail = prefs.getString('email') ?? '';
    _defaultPassword = prefs.getString('password') ?? '';

    setState(() {
      emailController.text = _defaultEmail;
      passwordController.text = _defaultPassword;
    });
  }
}
